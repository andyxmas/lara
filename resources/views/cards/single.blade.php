@extends('layout')

@section('content')
	<div class="container">

		<p>Card number {{ $card->id }}</p>

		<h2>{{ $card->title }}</h2>
		<ul class="list-group">
		@foreach ($card->notes as $note)
			<li class="list-group-item">
				{{ $note->body }}
				<span style="margin: 0 10px;" class="pull-right">{{ $note->user->name }}</span>
				<a class="pull-right" href="/notes/{{ $note->id }}/edit">Edit</a>
			</li>
		@endforeach
		</ul>

		<h3>Add Note</h3>

		<form method="POST" action="/cards/{{ $card->id }}/notes">
			{{ csrf_field() }}
			<div class="form-group">
					<textarea name="body" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default">Add Note</button>
			</div>

		</form>
	</div>
@stop
