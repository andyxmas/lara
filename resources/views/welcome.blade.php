@extends('layout')

@section('content')
	<h1>Welcome Page</h1>
	@unless (empty($people))
		There are some people
	@else
		here are no people
	@endunless

	@foreach ($people as $person)
		<li>{{ $person }}</li>
	@endforeach
@stop
