<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lara</title>

        <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}" />

    </head>
    <body>

			<ul>
				<li>
					<a href="/cards">Cards</a>
				</li>
			</ul>

			@yield('content')

		<script src="/js/app.js"></script>

    </body>
</html>
