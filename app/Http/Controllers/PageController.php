<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home() {
			$people = ['Matt', 'Steve', 'Dave'];
			return view('welcome', compact('people'));
		}

		public function about() {
			return view('pages.about');
		}
}
