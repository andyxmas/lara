<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Card;

class CardsController extends Controller
{
    public function index() {
			$cards = Card::all();

			return view('cards.index', [
				'cards' => $cards
			]);
		}

		public function show(Card $card) {

			$card->load('notes.user');
			// return $card;

			return view('cards.single', [
				'card' => $card
			]);
		}
}
